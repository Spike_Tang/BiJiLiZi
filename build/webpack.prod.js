const webpack = require('webpack');
const merge = require('webpack-merge');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    plugins: [
        // 打包工具需要先以来 babel-loader 不然会报错
        new UglifyJSPlugin({})
    ]
});