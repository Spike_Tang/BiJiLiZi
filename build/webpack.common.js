const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const config = require('../config/config.js');

module.exports = {
    entry: {
        app: './src/index.js',
        vendor: [
            'lodash'
        ]
    },
    devServer: {
        contentBase: './dist'
    },
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js'
        }
    },
    module: {
        rules: [{
            test: /\.(js|vue)$/,
            loader: 'eslint-loader',
            enforce: 'pre',
            include: [config.src],
            options: {
                formatter: require('eslint-friendly-formatter')
            }
        }, {
            test: /\.vue$/,
            use: ['vue-loader']
        }, {
            test: /\.css$/,
            use: ['style-loader', 'css-loader']
        }, {
            test: /\.less$/,
            use: ['style-loader', 'css-loader', 'less-loader']
        }, {
            test: /\.(png|svg|jpg|gif)$/,
            use: ['file-loader']
        }, {
            test: /\.js$/,
            use: ['babel-loader'],
            //打包除这个文件之外的文件
            exclude: config.node_modules,
            //打包包括的文件
            include: config.src
        }]
    },
    plugins: [
        new CleanWebpackPlugin([config.dist], {
            root: config.rootUrl
        }),
        new HtmlWebpackPlugin({
            title: '测试环境',
            filename: path.join(config.dist, '/index.html'),
            template: path.join(config.src, '/index.html')
        }),
        // 将框架方法单独合并 这个顺序一定要在common 之前
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor'
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'common' // 指定公共 bundle 的名称。
        })
    ],
    output: {
        // 开发环境中请不要使用 chunkhash 会冲突报错
        // filename: '[name].[chunkhash].bundle.js',
        filename: '[name].bundle.js',
        path: config.dist
    }
};