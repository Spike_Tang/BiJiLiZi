import Vue from 'vue'
import Vuex from 'vuex'

import page2 from './page2Module.js'

Vue.use(Vuex)
export default new Vuex.Store({
    modules: {
        page2: page2
    }
})
