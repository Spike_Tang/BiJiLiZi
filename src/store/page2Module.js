export default {
    state: {
        // false 对应 active 未完成  true 对应 completed 完成
        todos: [{
            text: 'text1',
            done: true
        }, {
            text: 'text2',
            done: false
        }]
    },
    getters: {

    },
    mutations: {
        addTodo (state, { text }) {
            state.todos.push({
                text,
                done: false
            })
        },
        deleteTodo (state, { todo }) {
            state.todos.splice(state.todos.indexOf(todo), 1)
        },
        clearCompleted (state) {
            state.todos = state.todos.filter(todos => !todos.done)
        },
        toggleTodo (state, { todo }) {
            todo.done = !todo.done
        }
    },
    actions: {

    }
}
