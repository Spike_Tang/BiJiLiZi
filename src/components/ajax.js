import Vue from 'vue'
import axios from 'axios'

function get (that) {
    console.log(that)
    axios.post('http://localhost:3000/code/find').then((res) => {
        console.log(res)
    })
}

function get1 (that) {
    axios.post('http://localhost:8081/package.json').then((res) => {
        console.log(res)
    })
}

// 全局混合 代码参考vuex 源码  mixin.js 中的 vuexInit 代码
Vue.mixin({
    beforeCreate () {
        const options = this.$options
        // store injection
        if (options.myAjax) {
            this.$myAjax = options.myAjax
        } else if (options.parent && options.parent.$myAjax) {
            this.$myAjax = options.parent.$myAjax
        }
    }
})

export default {
    get,
    get1
}
