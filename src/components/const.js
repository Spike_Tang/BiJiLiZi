import Vue from 'vue'
// 全局混合 代码参考vuex 源码  mixin.js 中的 vuexInit 代码
Vue.mixin({
    beforeCreate () {
        const options = this.$options
        // store injection
        if (options.STORE_CONFIG) {
            this.$STORE_CONFIG = typeof options.STORE_CONFIG === 'function'
                ? options.STORE_CONFIG()
                : options.STORE_CONFIG
        } else if (options.parent && options.parent.$STORE_CONFIG) {
            this.$STORE_CONFIG = options.parent.$STORE_CONFIG
        }
    }
})

const ADD_COUNT = 'ADD_COUNT'
const DEL_COUNT = 'DEL_COUNT'
const UPDATA_COUNT = 'UPDATA_COUNT'

export default {
    ADD_COUNT,
    DEL_COUNT,
    UPDATA_COUNT
}
