import Vue from 'vue'
import Router from 'vue-router'

import router1 from './page1.js'
import router2 from './page2.js'
import router3 from './page3.js'
import router4 from './page4.js'

Vue.use(Router)

var router = new Router({
    routes: [
        router1,
        router2,
        router3,
        router4
    ]
})

/*
to.matched.some( record => {} )
*/

// // 跳转之前
// router.beforeEach((to, from, next) => {
//     // 一定要next 执行下一步
//     console.log('all', to, from)
//     next()
// })

// // 跳转之后
// router.afterEach((to, from) => {
//     console.log('all', to, from)
// })

export default router
