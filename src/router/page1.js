import page1 from '../page/page1.vue'

import page3 from '../page/page3.vue'

export default {
    path: '/page1',
    name: 'page1',
    component: page1,
    // beforeEnter: (to, from, next) => {
    //     // 跳转之前
    //     // 一定要next 执行下一步
    //     console.log('page1 before')
    //     next()
    // },
    children: [
        {
            name: 'ES5_Array',
            path: 'page3',
            component: page3
        }
    ]
}
