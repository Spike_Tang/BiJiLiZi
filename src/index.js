import Vue from 'vue'
import router from './router/index.js'
// 如果没用用 default 倒出就用{}方式取变量
// import {store, STORE_CONFIG} from './store/index.js'
import store from './store/index.js'
import STORE_CONFIG from './components/const.js'

import App from './App.vue'

import './css/reset.css'
import './css/common.less'

import myAjax from './components/ajax.js'

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    myAjax,
    STORE_CONFIG,
    template: '<App/>',
    components: { App }
})
