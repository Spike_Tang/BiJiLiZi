const path = require('path');

module.exports = {
    rootUrl: path.resolve(__dirname, '..'),
    dist: path.resolve(__dirname, '../dist'),
    src: path.resolve(__dirname, '../src'),
    node_modules: path.resolve(__dirname, '../node_modules')
}
